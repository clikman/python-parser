#!/usr/bin/env python3

import datetime
import errno
import getopt
import queue
from multiprocessing import Process, Pipe
import os
import random
import stat
import sys
import time

from selenium import webdriver

from avito_parser_controller import AvitoActions, BrowserInstance
from db import db_session
from errors_handler import custom_logging_message
from models import Object


class Controller:
    """Launch worker processes, receive tasks from out and give send them to workers."""

    _FIFO = 'runtime.pipe'
    _FIFO_WRITE = 'runtime_wr.pipe'

    modes = ['checker', 'parser']

    children = {}

    def __init__(self, children_num=1):
        self._create_fifo()

        threads = {}
        for i in range(1, children_num + 1):
            parent_conn, child_conn = Pipe(True)  # True for bidirectional
            mode = self.modes[1] if i != 1 else self.modes[0]
            threads[i] = {'instance': self.make_child(i, child_conn, mode), 'pipe': parent_conn}
            self.children[i] = threads[i]

        for k in threads:
            # start the threads running
            threads[k]['instance'].daemon = True
            threads[k]['instance'].start()

        self.listener()

    def __del__(self):
        self._delete_fifo()

    def _create_fifo(self):
        try:
            # create fifo for out commands receiving
            os.mkfifo(self._FIFO)
        except OSError as oe:
            if oe.errno != errno.EEXIST:
                raise

    def _delete_fifo(self):
        try:
            os.remove(self._FIFO)
        except OSError:
            custom_logging_message('Deleting fifo has been failed!')

    def restore_fifo(self):
        self._delete_fifo()
        self._create_fifo()

    def make_child(self, number, q, mode):
        return Process(target=Worker, args=(number, q, mode))

    def listener(self):
        if self.children:
            while True:
                try:
                    time.sleep(random.random())
                    for k in self.children:
                        try:
                            # get workers' messages
                            message_flag = self.children[k]['pipe'].poll()
                            if message_flag:
                                message = self.children[k]['pipe'].recv()
                                custom_logging_message('Child {} send info parent: {}'.format(str(k), message))
                        except EOFError:
                            del self.children[k]
                        except queue.Empty:
                            pass
                        try:
                            # check child status
                            if self.children[k]['instance'].exitcode:
                                custom_logging_message('Attention! Child ' + k + ' is close.')
                            if not self.children[k]['instance'].is_alive():
                                custom_logging_message('Attention! Child ' + k + ' is not alive.')
                        except Exception:
                            pass
                    # check incoming commands
                    data = self.read_out()
                    if data == 'childs':
                        self.write_out(len(self.children))
                except queue.Empty:
                    pass
        custom_logging_message('Parsing has ended')

    def read_out(self):
        """Reading out commands via named pipe
        :return: data from pipe
        """
        try:
            state_fifo = stat.S_ISFIFO(os.stat(self._FIFO).st_mode)
            if state_fifo:
                fifo = os.open(self._FIFO, os.O_RDONLY)
            else:
                raise OSError('stat fifo is bad!')
            if fifo is not None:
                data = os.read(fifo, 128)
                if len(data) == 0:
                    pass
                else:
                    data = data.decode("utf-8").strip()
                if data:  # some commands have come
                    return data
            os.close(fifo)
        except OSError as exc:
            custom_logging_message('Read from general fifo error.')
            if exc.errno == errno.ENOENT:
                custom_logging_message('ENOENT')
            if exc.errno == errno.EAGAIN or exc.errno == errno.EWOULDBLOCK:
                custom_logging_message('EAGAIN or EWOULDBLOCK')
            self.restore_fifo()

        return False

    def write_out(self, message_text):
        data_send = bytearray(str(message_text), 'utf-8')
        try:
            server_file = os.open(self._FIFO_WRITE, os.O_WRONLY | os.O_NONBLOCK)
            os.write(server_file, data_send)
        except OSError as exc:
            if exc.errno == errno.EPIPE:
                custom_logging_message("Write general pipe EPIPE error")
            elif (exc.errno == errno.ENOENT):
                try:
                    os.mkfifo(self._FIFO_WRITE)
                    self.write_out(message_text)  # try again
                except OSError:
                    pass
            else:
                custom_logging_message(exc)

    def close_child(self, num):
        if self.children[num]:
            self.children[num].terminate()


class Worker(Controller):
    """Perform tasks from Controller. It can work as crawler and parser."""

    __q = None
    mode = None
    my_number = None
    driver = None
    status = None
    state = {}
    wait = False

    def __init__(self, num, q, mode):
        self.__q = q
        if not mode:
            mode = self.modes[1]
        self.mode = mode
        self.my_number = num
        self.about_me()

        rand = random.random()
        last_check = time.time()
        while True:
            try:
                time.sleep(rand)
                message_flag = self.__q.poll()
                if message_flag:
                    get_data = self.__q.recv()
                    message = "{} message: {} - getting and i working.".format(num, str(get_data[0]))
                    custom_logging_message(message)
                    self.__q.send(message)
                # every 5 seconds send message to parent that the process is alive
                if last_check + 5 < time.time():
                    self.__q.send(str(self.my_number) + ' alive' + str(self.__q.fileno()))
                    last_check = time.time()
                self.task()
            except queue.Empty:
                pass
            except Exception as err:
                custom_logging_message(err)

    def task(self):
        if not self.wait or self.wait < time.time():
            if hasattr(self, self.mode):
                work_as = getattr(self, self.mode)  # checker or parser
                work_as()
        else:
            self.status = 'wait'
            custom_logging_message("Child {} has status is '{}', state '{}'.".format(self.my_number, self.status,
                                                                                     self.state))

    def checker(self):
        """Checker role: It should round by pages after filtering (region, category,
        type deal, price, publisher) and gets list of objects.
        A round by region can begin from point of page.
        """
        time.sleep(random.random())
        try:
            for reg in AvitoActions.regions_work:
                dr = webdriver.PhantomJS('../phantomjs')
                br = BrowserInstance(dr, AvitoActions())
                scenario = br.resource_object
                scenario.parent_num = self.my_number
                scenario.checker(region=reg)
                dr.quit()

            # clear old resources
            self.clear_old()
        except Exception as err:
            custom_logging_message(str(err))
            dr.quit()

    def clear_old(self):
        old_objects = Object.query.filter(Object.status < 2).filter(
            Object.update_time < datetime.datetime.utcnow() - datetime.timedelta(days=3))
        if old_objects:
            for obj in old_objects:
                obj.delete()
            db_session.commit()

        return True

    def parser(self):
        """Parser role: get region data, delimit by parts and parse."""

        time.sleep(random.random())
        reg_count = len(AvitoActions.regions_work)
        region = self.my_number % reg_count
        region = AvitoActions.regions_work[region]
        try:
            dr = webdriver.PhantomJS('../phantomjs')
            br = BrowserInstance(dr, AvitoActions())
            scenario = br.resource_object
            scenario.parent_num = self.my_number
            scenario.parser(region=region)

            dr.quit()

        except Exception as err:
            custom_logging_message("Child {}. Disruption: {}".format(self.my_number, str(err)))
            dr.quit()

    def close(self):
        sys.exit(0)

    def write_to_general_pip(self, message):
        self.__q.send(message)

    def about_me(self):
        custom_logging_message("I'am child num: {}; mode: {}; ppid: {}; pid: {}".format(
            self.my_number, self.mode, os.getppid(), os.getpid()))

    def __repr__(self):
        return '<Worker %s - %s>'.format(self.my_number, self.mode)


if __name__ == "__main__":
    procnum = 1  # by default
    opts, args = getopt.getopt(sys.argv[1:], "p:d:v")
    if args and args[0] == 'help':
        print("help command for controller:")
        print("""Options:\n
            -p number   - amount of working processes
            """)
        sys.exit(0)
    for opt, arg in opts:
        if opt[1:] == 'p':
            custom_logging_message("will create workers:", arg, "\n")
            procnum = int(arg)

    controller = Controller(childs=procnum)
