import sys

import logging


def custom_logging_message(log_text=''):
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    line_num = tb.tb_line_num
    filename = f.f_code.co_filename
    logging.debug('{}. FILENAME {}, LINE {}. ERROR: "{}"): {}'.format(
        log_text, filename, line_num, exc_obj))
